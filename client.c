//
//  client.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 19/03/14.
//
//

#include "lib.h"
#define FILE_BLOCK_LEN 256

int
main(int argc, char* argv[])
{
    FILE *fp = NULL;
    RSA* rsa;
    struct crypto msg;
    unsigned int file_len = FILE_BLOCK_LEN;
    int digest_len = 20, i = 0, server = 0;
    unsigned char* key = (unsigned char*) "sessionkey";
    unsigned char* enckey;
    char *file_pem_pub = "pub.pem";
    uint32_t enckey_len;

    
    //Controllo degli argomenti
    if(argc != 2)
    {
		printf("Usage: client <file>\n");
		exit(EXIT_FAILURE);
    }
	
	//Allocazione del contesto di RSA
	OpenSSL_add_all_algorithms();
    rsa = RSA_new();  
	
	//Cifro la chiave di sessione con la chiave pubblica
	//e la invio al server
	fp = fopen(file_pem_pub, "r");
	rsa = PEM_read_RSAPublicKey(fp,&rsa,NULL,NULL);
	fclose(fp);
	
	enckey = (unsigned char*) calloc(1,RSA_size(rsa));
	enckey_len = RSA_public_encrypt(8,key,enckey,rsa,RSA_PKCS1_PADDING);

	
    //Connetto al server  
    server = connectToServer("127.0.0.1",1234);
    
    //Invio la chiave di sessione
    sendMessage(server, (char*) enckey, enckey_len);
    
    //Azzero la struttura
    memset(&msg,0,sizeof(msg));
    
    //Inserisco la chiave nella struttura
    stpncpy(msg.k,(char *)key,8);    
    
    //Allocazione dello spazio per leggere il contenuto del file
    msg.pt = (char*) calloc(file_len,sizeof(char));
    
    //Apertura file
    fp = fopen(argv[1],"r");
    
    //Controllo se l'apertura è andata a buon fine
    if(fp == NULL)
    {
        perror("Error while opening the file.\n");
        close(server);
        exit(EXIT_FAILURE);
    }
    
    //Leggo tutti i byte fino a alla fine del file
    for(i=0; (msg.pt[i] = fgetc(fp)) != EOF; i++)
    {
      if(i == file_len-1)
      {
		file_len += FILE_BLOCK_LEN;
		msg.pt = (char*) realloc(msg.pt,file_len);
      }
    }
    
    //Fine stringa al posto di EOF
    msg.pt[i] = '\0';
    
    //Chiudo il file
    fclose(fp);
    
    /* Output of the original message */
    printf("\nOriginal message:\n%s\n",msg.pt);
    
    //Calcolo della dimensione della stringa
    msg.pt_len = strlen(msg.pt)+1;
    
    //Calcolo l'hash e lo concateno al plain text
    hash_sha1(msg.pt,msg.pt_len,&msg.pt[msg.pt_len]);
    
    //Aggiorno la dimesione del plain text
    msg.pt_len += digest_len;

    //Rilascio dello spazio non utilizzato nel plaintext
    msg.pt = (char *) realloc(msg.pt, (size_t)msg.pt_len);
	
    //Cifratura del messaggio + hash
    my_encrypt(&msg);
    
    //Invio file
    sendMessage(server, msg.ct, msg.ct_len);

    
    //Deallocazione messaggio e chiusura app
    free(msg.pt);
    free(msg.ct);
    RSA_free(rsa);
    
    //chiudo la connessione con il server
    close(server);
    
    printf("\nClosing client...\n");
    
    return 0;
}