//
//  lib.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 22/03/14.
//
//

#include "lib.h"

/* stampa un byte in esadecimale */
void printbyte(char b) {
	char c;
	c = b;
	c = c >> 4;
	c = c & 15; printf("%X", c); c = b;
	c = c & 15; printf("%X:", c);
}

void
enterPassword(char dest[])
{
    char *buf = calloc(255,sizeof(char));
    printf("\nPassword (no spaces): ");
    scanf("%s",buf);
    stpncpy(dest,buf,8);
    free(buf);
}


void
my_encrypt(struct crypto *msg)
{
    int nc; /* amount of bytes [de]crypted at each step */
	int nctot; /* total amount of encrypted bytes */
	int ct_ptr; /* first available entry in the buffer */
    
    /* Context allocation */
	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX *) calloc(1,sizeof(EVP_CIPHER_CTX));
	
	/* Context initialization */
	EVP_CIPHER_CTX_init(ctx);
	
	/* Context setup for encryption */
	EVP_EncryptInit(ctx, EVP_des_ecb(), NULL, NULL);
    
	/* Output of the encryption key size */
	printf("Key size: %d\n", EVP_CIPHER_key_length(EVP_des_ecb()));
	
	/* Output of the block size */
	printf("Block size: %d\n", EVP_CIPHER_CTX_block_size(ctx));
    
    /* Encryption key set up */
	EVP_EncryptInit(ctx, NULL, (unsigned char *) msg->k, NULL);
	
	/* Buffer allocation for the encrypted text */
    msg->ct_len = msg->pt_len + EVP_CIPHER_CTX_block_size(ctx);
	msg->ct = (char *) calloc(msg->ct_len,sizeof(char));
    
    /* Encryption */
	nc = 0;
	nctot = 0;
	ct_ptr = 0;
	EVP_EncryptUpdate(ctx, (unsigned char *) msg->ct, &nc, (unsigned char *) msg->pt, msg->pt_len);
	ct_ptr += nc;
	nctot += nc;
	EVP_EncryptFinal(ctx, (unsigned char *) &msg->ct[ct_ptr], &nc);
	nctot += nc;
    
    msg->ct_len = (uint32_t) nctot;
    
	printf("Message size: %d\n", msg->pt_len);
	printf("Ciphertext size: %d\n", msg->ct_len);
    
    //deallocazione del context
    free(ctx);
}

void
my_decrypt(struct crypto *msg)
{
    int nc; /* amount of bytes [de]crypted at each step */
	int nctot; /* total amount of encrypted bytes */
	//int i; /* index */
	int ct_ptr; /* first available entry in the buffer */
    
    /* Context allocation */
	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX *) calloc(1,sizeof(EVP_CIPHER_CTX));
    
    /* Decryption context initialization */
	EVP_CIPHER_CTX_init(ctx);
	EVP_DecryptInit(ctx, EVP_des_ecb(), (unsigned char *) msg->k, NULL);
    
	msg->pt_len = msg->ct_len + EVP_CIPHER_CTX_block_size(ctx);
	msg->pt = (char *) calloc(msg->pt_len,sizeof(char));
    
    /* Decryption */
	nc = 0;
    nctot = 0;
	ct_ptr = 0;
	EVP_DecryptUpdate(ctx, (unsigned char *) msg->pt, &nc, (unsigned char *) msg->ct, msg->ct_len);
	ct_ptr += nc;
	nctot += nc;
	EVP_DecryptFinal(ctx, (unsigned char *) &msg->pt[ct_ptr], &nc);
	nctot += nc;
    
    msg->pt_len = (uint32_t) nctot;
    
    //deallocazione del context
    free(ctx);
}

void
hash_sha1(char *msg, uint32_t msg_len, char digest[])
{
    unsigned int digest_len = 20, i = 0;
    EVP_MD_CTX* mdctx;
    
    OpenSSL_add_all_digests();
    const EVP_MD* md = EVP_get_digestbyname("sha1");
    mdctx = (EVP_MD_CTX *) calloc(1,sizeof(EVP_MD_CTX));
    
    /* Context initialization */
    EVP_MD_CTX_init(mdctx);
    EVP_DigestInit(mdctx, md);
    
    /* Calcolo digest */
    EVP_DigestUpdate(mdctx,msg,msg_len);
    EVP_DigestFinal_ex(mdctx,(unsigned char *) digest,&digest_len);
    
    printf("\nSHA-1 =>  ");
	
	for (i = 0; i < digest_len ; i++)
		printbyte(digest[i]);
		
	printf("\n\n");
    
    //Deallocazione strutture dati
    EVP_MD_CTX_cleanup(mdctx);
    free(mdctx);
}


int
connectToServer(char *ip, unsigned int port)
{
	struct sockaddr_in srv_addr;
	int ret, sk;
	
	if(ip == NULL)
	{
		//inserimento indirizzo IP e porta
		ip = (char *) calloc(20,sizeof(char)); //non viene mai deallocato!!
    	printf("Type IP address and port: ");
    	scanf("%s %i",ip,&port);
    }

	//creazione del socket TCP
    sk = socket(AF_INET, SOCK_STREAM, 0);
    
    //azzeramento della struttura
    memset(&srv_addr, 0, sizeof(srv_addr));
    
    //inserimento nella struttura dell'Ip e porta
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(port);
    ret = inet_pton(AF_INET,ip, &srv_addr.sin_addr);
    
    //connessione al server
    ret = connect(sk, (SA *) &srv_addr, sizeof(srv_addr));
    
    //controllo connessione
    if(ret == -1)
    {
        perror("\nconnectToServer() error");
        exit(EXIT_FAILURE);
    }
    
    printf("\nConnection to server established\n");
    
    return sk;
}

void
sendMessage(int sk, char *msg, uint32_t msg_len)
{
	int ret;
	uint32_t msg_len_net = 0;
	
	//Metto in big-endian (network format)
	msg_len_net = htonl(msg_len);
    
    //invio della dimensione del messaggio
    ret = send(sk, (void*) &msg_len_net, sizeof(uint32_t),0);
    
    //controllo invio richiesta
    if(ret == -1)
    {
        perror("\nSending message length error");
        exit(EXIT_FAILURE);
    }
    
    //invio del messaggio
	ret = send(sk, (void*) msg, msg_len,0);
	
	//controllo invio richiesta
	if(ret == -1)
	{
		perror("\nSending message error");
		exit(EXIT_FAILURE);
	}
	
	//Stampa informazioni e chiusura connessione
	//printf("\nMessage sent successfully\n");
}

void
receiveMessage(int sk, char **msg, uint32_t *msg_len)
{
	int ret;
	uint32_t msg_len_net = 0;
	     
	//arriva la dimensione
    ret = recv(sk, (void *) &msg_len_net, sizeof(uint32_t), 0);
	if(ret == -1)
	{
		perror("\nReceiving message length error");
		exit(EXIT_FAILURE);
	}
	
	//Converto nel formato dell'host (little endian su x86_64)
	*msg_len = ntohl(msg_len_net);
	
	//alloco lo spazio per il messaggio
    *msg = (char*) calloc(*msg_len,sizeof(char));
    
	//ricezione del messaggio
	ret = recv(sk, (void *) *msg, *msg_len, MSG_WAITALL);
	if(ret == -1)
	{
		perror("\nReceiving message error");
		exit(EXIT_FAILURE);
	}
}