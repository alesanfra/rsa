#include "lib.h"

int
main(void)
{
	RSA *rsa;
	FILE *fp;
	char *file_pem = "priv.pem";
	char *file_pem_pub = "pub.pem";
	char *kstr = "password";
	const unsigned int digest_len = 20;
	unsigned char digest[digest_len];
	int bits = 1024;
	unsigned long exp = RSA_F4;
	
	
	//generazione coppia di chiavi
	rsa=RSA_generate_key(bits,exp,NULL,NULL);
	
	//genero la chiave di cifratura di priv.pem
	hash_sha1(kstr,strlen(kstr),(char *)digest);
	
	//Salvo la chiave privata nel file prim.pem
	fp = fopen(file_pem, "w");
	PEM_write_RSAPrivateKey (fp,rsa,EVP_des_ede3_cbc(),digest,digest_len,NULL,NULL);
	fclose(fp);
	
	//Salvo la chiave pubblica nel file pub.pem
	fp = fopen(file_pem_pub, "w");
	PEM_write_RSAPublicKey(fp, rsa);
	fclose(fp);
	
	//Dealloco il context
	RSA_free(rsa);
}