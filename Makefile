CC = gcc
CFLAGS = -Wall -Wno-deprecated
LIB = -lcrypto

all: keygen client server

clean: clear all

keygen: key-gen.o lib.o
	$(CC) -o key-gen $^ $(LIB)

client: client.o lib.o
	$(CC) -o client $^ $(LIB)
	
server: server.o lib.o
	$(CC) -o server $^ $(LIB)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

clear:
	rm -rf *.o server client key-gen
