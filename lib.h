//
//  lib.h
//  sec-lab2
//
//  Created by Alessio Sanfratello on 22/03/14.
//
//

#ifndef sec_lab2_lib_h
#define sec_lab2_lib_h

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#define SA struct sockaddr

struct crypto {
	char k[8];
    char *pt;
    char *ct;
    uint32_t pt_len;
    uint32_t ct_len;
};

void printbyte(char);
void enterPassword(char dest[]);

void my_encrypt(struct crypto *msg);
void my_decrypt(struct crypto *msg);
void hash_sha1(char *msg, uint32_t msg_len, char digest[]);

int connectToServer(char *ip, unsigned int port);

void sendMessage(int sk, char *msg, uint32_t msg_len);
void receiveMessage(int sk, char **msg, uint32_t *msg_len);

#endif
